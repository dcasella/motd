# MOTD

![Screenshot](screenshot.png)

Simple pure Bash script for displaying basic system and application information.

## Installation

```
$ curl https://gitlab.com/dcasella/motd/raw/master/motd --output motd
$ chmod +x motd
# mv motd /usr/local/bin
```

### Debian-based

File: `/etc/update-motd.d/00-motd` (and make it executable)

```bash
#!/bin/sh

ERROR_LOG_PATH='/var/log/motd.err.log'
rm -f "${ERROR_LOG_PATH}"

CONFIG_PATH='/etc/motd.json'

if [ ! -f "${CONFIG_PATH}" ]; then
    echo "MOTD error. Configuration file ${CONFIG_PATH} does not exist"
    exit
fi

motd "${CONFIG_PATH}" 2> "${ERROR_LOG_PATH}"

if [ -s "${ERROR_LOG_PATH}" ]; then
    echo "MOTD error. Check ${ERROR_LOG_PATH}"
fi
echo

```

Where `/etc/motd.json` is the path to the desired JSON configuration file.

### Dependencies

- [bash](https://www.gnu.org/software/bash/) >= 4
- [jq](https://stedolan.github.io/jq/)

## Usage

> `motd [FLAG...] [OPTION...] CONFIGFILE`

### Flags

All flags exit the program.  
Flags are parsed in the displayed order.

> `-h`  
> Display the help message

### Parameters

> `CONFIGFILE`  
> Configuration file describing the enabled functionalities

## Configuration

The script is configurable via JSON file.

Top level configuration settings (such as `uptime` and `services`) can be omitted to disable a specific functionality.  
Duplicate settings are not allowed.

### Structure

```json
{
    "uptime": {
        "prefix": "[optional] uptime prefix "
    },
    "ssl_certificates": [
        {
            "path": "/path/to/certificate",
            "name": "[optional] displayed name"
        },
        ...
    ],
    "services": [
        {
            "command": "command to retrieve exit status",
            "name": "[optional] displayed name",
            "column": "[optional] left/center/right"
        },
        ...
    ],
    "load_avg": {
        "prefix": "[optional] load_average prefix "
    },
    "memory": true,
    "filesystems": [
        {
            "path": "/path/to/block/device",
            "name": "[optional] displayed name",
            "column": "[optional] left/right"
        },
        ...
    ]
}
```

Available functionalities in detail:

### Uptime

Display the uptime.

**Type**: `Object`

**Parameter**: `uptime`

**Object parameters**:

- `prefix`  
  *Optional*  
  **Type**: `String`  
  **Default**: Empty string  
  Prepend the string defined in `prefix` to the output of `uptime`

#### `Examples` Uptime

```json
{
    "uptime": {}
```

Or

```json
{
    "uptime": {
        "prefix": "up "
    }
```

### SSL Certificates

Display the expiration status of the selected SSL certificates.

**Type**: `Array`

**Parameter**: `ssl_certificates`

**Array elements type**: `Object`

**Array element parameters**:

- `path`  
  **Required**  
  **Type**: `String`  
  SSL Certificate path to monitor
- `name`  
  *Optional*  
  **Type**: `String`  
  **Default**: parent folder retrieved from `path`  
  SSL Certificate displayed name

#### `Examples` SSL Certificates

```json
{
    "ssl_certificates": [
        {
            "path": "/path/to/ssl/domain/1.pem"
        },
        {
            "path": "/path/to/ssl/domain/2.pem",
            "name": "display-name-2"
        }
    ]
```

The name displayed for the certificate with path `/path/to/ssl/domain/1.pem` will be: `domain`.

### Services

Display the status of the selected systemd services.

**Type**: `Array`

**Parameter**: `services`

**Array elements type**: `Object`

**Array element parameters**:

- `command`  
  **Required**  
  **Type**: `String`  
  Command ran to monitor the service status
- `name`  
  *Optional*  
  **Type**: `String`  
  **Default**: `${command}`  
  Service displayed name
- `column`  
  *Optional*  
  **Type**: `String`  
  **Default**: `left`  
  **Values**: `left`, `center`, `right`  
  Service column display choice

#### `Examples` Services

```json
{
    "services": [
        {
            "command": "systemctl status service-name-1"
        },
        {
            "command": "service status service-name-2",
            "name": "display-name-2"
        },
        {
            "command": "service status service-name-3",
            "column": "center"
        },
        {
            "command": "service status service-name-4",
            "name": "display-name-4",
            "column": "right"
        }
    ]
```

### Load average

Display the load average (1min, 5min, 15min).

**Type**: `Object`

**Parameter**: `load_avg`

**Object parameters**:

- `prefix`  
  *Optional*  
  **Type**: `String`  
  **Default**: Empty string  
  Prepend the string defined in `prefix` to the output of `load_avg`

#### `Examples` Load average

```json
{
    "load_avg": {}
```

Or

```json
{
    "load_avg": {
        "prefix": "load "
    }
```

### Memory

Display the status of the memory.

**Type**: `Boolean`

**Parameter**: `memory`

#### `Examples` Memory

```json
{
    "memory": true
```

### Filesystems

Display the status of the selected filesystem.

**Type**: `Array`

**Parameter**: `filesystems`

**Array elements type**: `Object`

**Array element parameters**:

- `path`  
  **Required**  
  **Type**: `String`  
  Filesystem path to monitor
- `name`  
  *Optional*  
  **Type**: `String`  
  **Default**: `${path}`  
  Filesystem displayed name
- `column`  
  *Optional*  
  **Type**: `String`  
  **Default**: `left`  
  **Values**: `left`, `right`  
  Filesystem column display choice

#### `Examples` Filesystems

```json
{
    "filesystems": [
        {
            "path": "/dev/sda1"
        },
        {
            "path": "/dev/sdx2",
            "name": "display-name-2"
        },
        {
            "path": "/dev/sdx3",
            "column": "right"
        },
        {
            "path": "/dev/sdx4",
            "name": "display-name-4",
            "column": "right"
        }
    ]
```

## Credits

Design and idea taken from [FalconStats](https://github.com/Heholord/FalconStats/tree/master), written in NodeJS, and [panda-motd](https://github.com/taylorthurlow/panda-motd), written in Ruby.
